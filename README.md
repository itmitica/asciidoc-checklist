# asciidoc-workflow

how to create asciidoc documents


## edit asciidoctor files

use gitlab editors: "Edit" or "Web IDE"


## preview asciidoctor files

* install "Asciidoctor.js Live Preview" extension in Chrome

* select and "Open raw" `.adoc` files


## convert asciidoctor files to html, pdf

### clone a repository / pull updates

```shell
git clone https://gitlab.com/itmitica/asciidoc-checklist.git
# git pull origin
```

### start an interactive asciidoctor docker container in the repository folder

```shell
sudo docker run -it -v ~/asciidoc-checklist/:/documents/ asciidoctor/docker-asciidoctor
```

### convert to html, pdf

```shell
6e0b89e2591a:/documents# asciidoctor plan-aprilie-2023.adoc
6e0b89e2591a:/documents# asciidoctor-pdf plan-aprilie-2023.adoc
```

### push new files to repository

```shell
cd ./asciidoc-cheklist
git add .
git commit -m "html, pdf"
git push origin main
```

### preview html files on gitlab
```shell
# At the root of this repo, add a file called .gitlab-ci.yml containing the following lines:
pages:
 script:
 - mkdir .public
 - cp -r * .public
 - mv .public public
 artifacts:
    paths:
    - public

# The file is now available at
https://itmitica.gitlab.io/asciidoc-checklist/plan-aprilie-2023.html
```